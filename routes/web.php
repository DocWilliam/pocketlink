<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\JWTController;
use Firebase\JWT\JWT;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['secret','cors'])->get('/token/{token}',[JWTController::class,'token'])->name('token');

Route::post('/uploadimage',[MainController::class,'uploadImage'])->name('uploadimage');

Route::post('/uploadlink',[MainController::class,'uploadLink'])->name('uploadlink');
