<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\JWTController;
use Firebase\JWT\JWT;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/user',[MainController::class, 'user'])->name('user');

Route::post('/pockets',[MainController::class, 'pockets'])->name('pockets');

Route::post('/pocketlinks',[MainController::class, 'pocketLinks'])->name('pocketlinks');

Route::post('/userlinks',[MainController::class, 'userLinks'])->name('userlinks');

Route::post('/handleexists',[MainController::class, 'handleExists'])->name('handleexists');
