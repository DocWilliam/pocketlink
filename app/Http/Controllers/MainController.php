<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Pocket;

class MainController extends Controller
{
    public function user(Request $request){
        return User::where('handle',$request->handle)->first();
    }

    public function pockets(Request $request){
        return User::find($request->id)->pockets()->get();
    }

    public function pocketLinks(Request $request){
        return Pocket::find($request->id)->links()->get();
    }

    public function userLinks(Request $request){
        return User::find($request->id)->pockets()->get();
    }

    public function handleExists(Request $request){
        $user = new User;

        return array('exists'=>$user->handleExists($request->handle));
    }

    public function uploadImage(Request $request){
        //
    }

    public function uploadLink(Request $request){
        //
    }

    
}
